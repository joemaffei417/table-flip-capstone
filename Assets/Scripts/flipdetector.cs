using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flipdetector : MonoBehaviour
{
    public GameObject parent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {

        //Set up to work with a "floor" layer eventually so that the tables explode when they hit the floor on any level, or on multi floor levels
        if (other.name == "Plane") 
        {
            Destroy(parent);
        }
    }
}
